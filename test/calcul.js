'use strict';

const assert = require('assert');

const calcul = require('../calc/calculator');

describe('CALCUL', function() {
    describe('#makeSum(arg1_nb1, arg2_nb2)', function() {
        it('should return 4', async function() {
            const sumRes = await calcul.makeSum(2, 2);
            assert.equal(sumRes, 4);
        });
    });

    describe('#makeDivide(arg1_nb1, arg2_nb2)', function() {
        it('should return 2', async function() {
            const divideRes = await calcul.makeDivide(4, 2);
            assert.equal(divideRes, 2);
        });
    });
});
