'use strict';

const express = require('express');

const calculator = require('./calc/calculator');

// Constants
const PORT = 8080;
const HOST = '0.0.0.0';

// App
const app = express();


app.get('/', async(req, res) => {
    res.send('Hello world\n');
});


app.get('/sum', async (req, res, next) => {
    const sum = await calculator.makeSum(10,3);

    res.status(200).json({
        http_code: 200,
        data: sum
    })
});

app.get('/divide', async(req,res,next) => {
    const divide = await calculator.makeDivide(10,3);

    res.status(200).json({
        http_code: 200,
        data: divide
    })
});





app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
