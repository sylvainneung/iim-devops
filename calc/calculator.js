const calculator = {

    makeSum: (nb1, nb2) => {
        return new Promise(((resolve, reject) => {
            resolve(nb1 + nb2);
        }));
    },

    makeDivide: (nb1, nb2) => {
        return new Promise(((resolve, reject) => {
            resolve(Math.floor(nb1 / nb2));
        }));
    }
};


module.exports = calculator;
